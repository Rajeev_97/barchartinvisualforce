
public with sharing class BarChartHelper { 
    private static List&lt;String&gt; months=new List&lt;String&gt; { 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' };
    public List&lt;Data&gt; getChartData() { 
        Map&lt;Integer, Data&gt; dataByMonth = new Map&lt;Integer, Data&gt;(); 
        List&lt;Data&gt; chartData=new List&lt;Data&gt;(); 
        DateTime startDT=DateTime.newInstance(Date.today().addYears(-1).toStartOfMonth(), Time.newInstance(0, 0, 0, 0)); 
        DateTime endDT=DateTime.newInstance(Date.today(), Time.newInstance(23, 59, 59, 999)); 
        Integer startMonth=startDT.date().month()-1;
        for (Integer idx=0; idx&lt;12; idx++){
            Integer monthNo=Math.mod(startMonth+idx, 12);
            Data theData=new Data(months.get(monthNo));
            dataByMonth.put(monthNo, theData);
            chartData.add(theData);
        } 
        for (Opportunity opp : [select id, CloseDate, Amount from Opportunity where IsClosed = true and IsWon =     true and CloseDate&gt;=:startDT.date() and CloseDate&lt;=:endDT.date()]) {
            Data cand=dataByMonth.get(opp.CloseDate.month()-1); cand.oppTotal+=opp.Amount;
        }
      
        for (Integer idx=0; idx&lt;12; idx++) {
            Data cand=dataByMonth.get(idx);
            if (0.0==cand.oppTotal) {
                cand.oppTotal=Math.random()*750000;
            }
        }
        return chartData;
    }
    public class Data {
        public String name { get; set; } 
        public Decimal oppTotal { get; set; } 
        public Data(String name) {
            this.name = name; this.oppTotal = 0.0; 
        }
    }
}